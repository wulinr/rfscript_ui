*** Settings ***
Library           SeleniumLibrary

*** Test Cases ***
login
    open browser    http://47.105.115.80:8080/blog/    headlesschrome
    wait until page contains element    //*[@name="username"]
    input text    //*[@name="username"]    
    input password    //*[@name="password"]    
    click element    //*[@lay-filter="login"]
    sleep    3
    Comment    wait until page contains element    //*[@id="yhgl"]
    wait until page contains element    //*[@id="yhgl"]

login_fail
    open browser    http://47.105.115.80:8080/blog/    headlesschrome
    wait until page contains element    //*[@name="username"]
    input text    //*[@name="username"]    
    input password    //*[@name="password"]  
    click element    //*[@lay-filter="login"]
    sleep    3
    Comment    wait until page contains element    //*[@id="yhgl"]
    page should not contain    //*[@id="yhgl11"]
